<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es> and Jorge J. Gomez-Sanz
 *
 */
if (!defined('STATUSNET')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/util.php';

class TaskPlugin extends Plugin {

    function onCheckSchema() {
        $schema = Schema::get();

        $schema->ensureTable('task', Task::schemaDef());
        $schema->ensureTable('task_grader', Task_Grader::schemaDef());

        return true;
    }

    function onInitializePlugin() {
        // A chance to initialize a plugin in a complete environment
    }

    function onCleanupPlugin() {
        // A chance to cleanup a plugin at the end of a program.
    }

    function onRouterInitialized($m) {
        $m->connect('main/tasks', array('action' => 'taskcreate'));
        $m->connect('main/tasks/', array('action' => 'newnoticetask'));
        return true;
    }

    function onEndToolsLocalNav($action) {

        $actionName = $action->trimmed('action');

        $user = common_current_user();
        if (!empty($user)) {

            if ($actionName === 'taskcreate')
                $action->elementStart('li', array('id' => 'nav_task', 'class' => 'current'));
            else
                $action->elementStart('li', array('id' => 'nav_task'));

            $action->elementStart('a', array('href' => common_local_url('taskcreate'), 'title' => _m('Class tasks')));
            $action->text(_m('Tasks'));

            // Si es alumno, mostramos las tareas pendientes.
            if (!$user->hasRole('grader')) {

                // Llamamos a la función que obtenga el numero de tareas
                $number = Task::getNumberPendingTasks($user->id);

                if ($number != 0)
                    $action->element('span', 'pending-tasks-number', $number);
            }

            $action->elementEnd('a');
            $action->elementEnd('li');
        }

        return true;
    }

    function onPluginVersion(array &$versions) {
        $versions[] = array('name' => 'Task',
            'version' => 1.0,
            'author' => 'Alvaro Ortego',
            'rawdescription' =>
            _m('A plugin to generate homeworks.'));
        return true;
    }

    function onAutoload($cls) {

        $dir = dirname(__FILE__);

        switch ($cls) {

            case 'TaskcreateAction':
            case 'NewnoticetaskAction':
                include_once $dir . '/actions/' . $cls . '.php';
                return false;
            case 'InitForm':
            case 'NoticeTaskForm':
            case 'CancelForm':
            case 'DeleteForm':
            case 'ReopenForm':
                include_once $dir . '/lib/' . $cls . '.php';
                return false;
            case 'Task':
            case 'Task_Grader':
                include_once $dir . '/classes/' . $cls . '.php';
                return false;
                break;
            default:
                return true;
        }
    }

    function onEndShowStyles($action) {
        $action->cssLink($this->path('css/task.css'));
        return true;
    }

    function onEndShowScripts($action) {
        $action->script($this->path('js/task.js'));
        

        
        $action->inlineScript('
          

$( ".tasky" ).each(function () {
                        var form = $(this);
                       
            SN.U.NoticeLocationAttach(form);
            SN.U.FormNoticeUniqueID(form);
            SN.U.FormNoticeXHR(form);
            $(form).ajaxForm({success:function(data, textStatus){
              form.find(".form_response").remove();
              var errorResult = $("#" + SN.C.S.Error, data);
                    if (errorResult.length > 0) {
                        SN.U.showFeedback(form, "error", errorResult.text());
                    } else {
                        SN.U.showFeedback(form, "form_response","all ok");
                        form.replaceWith($("li",data).text());
                    }  
            }});
            SN.U.FormNoticeEnhancements(form);
            SN.U.NoticeDataAttach(form);                                  
                });');
        return true;
    }  

}
