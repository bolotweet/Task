<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Jorge J. Gomez Sanz <jjgomez@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/noticelist.php';
require_once INSTALLDIR . '/lib/mediafile.php';

class NewnoticetaskAction extends NewnoticeAction {

    protected function doPost() {
        $result = parent::doPost();
        if (isset($result)) {
            $user=$this->scoped->getUser();
            $taskid = $this->trimmed('taskid');
            $group = User_group::getKV('id', $this->trimmed('groupid'));
            Task::completeTask($user->id, $taskid);
        }
    }

    protected function showContent() {
        if ($this->getInfo() && $this->stored instanceof Notice) {
            $this->raw("<ul><li><p>Notice posted</p></li></ul>"); // to ensure proper text node replacement at the client side
        } elseif (!$this->getError()) {
            $this->raw("<ul><li><p>Notice posted2</p></li></ul>"); // to ensure proper text node replacement at the client side
        }
    }

}
