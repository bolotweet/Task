# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-07-11 18:48+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: scripts/updateHistorical.php:30 actions/TaskcreateAction.php:218
msgid "Iniciada"
msgstr ""

#: scripts/updateHistorical.php:32 actions/TaskcreateAction.php:220
msgid "Cancelada"
msgstr ""

#: scripts/updateHistorical.php:41
msgid "Fecha"
msgstr ""

#: scripts/updateHistorical.php:42 actions/TaskcreateAction.php:231
msgid "Tag:"
msgstr ""

#: scripts/updateHistorical.php:43 actions/TaskcreateAction.php:232
msgid "Completada:"
msgstr ""

#: scripts/updateHistorical.php:51 lib/CancelForm.php:43
msgid "Cancelar"
msgstr ""

#: scripts/updateHistorical.php:51 lib/CancelForm.php:44
msgid "Cancela esta tarea"
msgstr ""

#: scripts/updateHistorical.php:57 lib/ReopenForm.php:45 lib/InitForm.php:80
msgid "Iniciar"
msgstr ""

#: scripts/updateHistorical.php:57
msgid "Reabre esta tarea"
msgstr ""

#: lib/ReopenForm.php:46
msgid "Reabre esta tarea."
msgstr ""

#. TRANS: Form legend for notice form.
#: lib/NoticeTaskForm.php:172
msgid "Enviar una noticia"
msgstr ""

#. TRANS: Title for notice label. %s is the user's nickname.
#: lib/NoticeTaskForm.php:185
#, php-format
msgid "What's up, %s?"
msgstr ""

#. TRANS: Input label in notice form for adding an attachment.
#: lib/NoticeTaskForm.php:218
msgid "Attach"
msgstr ""

#. TRANS: Title for input field to attach a file to a notice.
#: lib/NoticeTaskForm.php:223
msgid "Attach a file."
msgstr ""

#. TRANS: Button text for sending notice.
#: lib/NoticeTaskForm.php:258
msgctxt "BUTTON"
msgid "Enviar"
msgstr ""

#: lib/DeleteForm.php:43
msgid "Borrar"
msgstr ""

#: lib/DeleteForm.php:44
msgid "Borrar esta tarea"
msgstr ""

#: lib/InitForm.php:81
msgid "Crea una tarea para este grupo"
msgstr ""

#: lib/InitForm.php:90
msgid "Añade un tag relacionado con la tarea"
msgstr ""

#: TaskPlugin.php:53
msgid "Tareas de Clase"
msgstr ""

#: TaskPlugin.php:54 actions/TaskcreateAction.php:360
msgid "Tareas"
msgstr ""

#: TaskPlugin.php:78
msgid "A plugin to generate homeworks."
msgstr ""

#. TRANS: Page title for sending a new notice.
#: actions/NewnoticetaskAction.php:34
msgctxt "TITLE"
msgid "New notice"
msgstr ""

#. TRANS: Error message displayed when trying to perform an action that requires a logged in user.
#: actions/NewnoticetaskAction.php:53 actions/TaskcreateAction.php:54
msgid "Not logged in."
msgstr ""

#. TRANS: Client error displayed when the number of bytes in a POST request exceeds a limit.
#. TRANS: %s is the number of bytes of the CONTENT_LENGTH.
#: actions/NewnoticetaskAction.php:60
#, php-format
msgid ""
"The server was unable to handle that much POST data (%s byte) due to its "
"current configuration."
msgid_plural ""
"The server was unable to handle that much POST data (%s bytes) due to its "
"current configuration."
msgstr[0] ""
msgstr[1] ""

#. TRANS: Client error displayed trying to send a notice without content.
#: actions/NewnoticetaskAction.php:101
msgid "Mensaje vacío!!"
msgstr ""

#. TRANS: Page title after sending a notice.
#: actions/NewnoticetaskAction.php:165
msgid "Notice posted"
msgstr ""

#: actions/NewnoticetaskAction.php:168
msgid "Tarea completada correctamente, mensaje publicado."
msgstr ""

#. TRANS: Page title after an AJAX error occurs on the send notice page.
#: actions/NewnoticetaskAction.php:188
msgid "Ajax Error"
msgstr ""

#: actions/TaskcreateAction.php:67
msgid "Solo letras y numeros"
msgstr ""

#: actions/TaskcreateAction.php:70
msgid "Hay que escribir un tag"
msgstr ""

#: actions/TaskcreateAction.php:111 actions/TaskcreateAction.php:133
#: actions/TaskcreateAction.php:152
msgid "Disfavor favorite"
msgstr ""

#: actions/TaskcreateAction.php:181
msgid "No tiene ningún grupo asociado."
msgstr ""

#: actions/TaskcreateAction.php:196
msgid "Fecha: "
msgstr ""

#: actions/TaskcreateAction.php:205
msgid "Histórico de tareas"
msgstr ""

#: actions/TaskcreateAction.php:230
msgid "Fecha:"
msgstr ""

#: actions/TaskcreateAction.php:251
msgid "Todavía no ha iniciado ninguna tarea."
msgstr ""

#: actions/TaskcreateAction.php:270
msgid "Enhorabuena, no tienes ninguna tarea pendiente."
msgstr ""

#: actions/TaskcreateAction.php:294
msgid "Sí"
msgstr ""

#: actions/TaskcreateAction.php:295
msgid "No"
msgstr ""

#. TRANS: Title.
#: actions/TaskcreateAction.php:349
msgid "Add to favorites"
msgstr ""
