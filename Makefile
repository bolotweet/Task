# Warning: do not transform tabs to spaces in this file.

all : translations

plugin_mo = $(patsubst %.po,%.mo,$(wildcard locale/*/LC_MESSAGES/*.po))
plugin_pot = $(wildcard locale/*/LC_MESSAGES/*.po)
pot_file= $(wildcard locale/*.pot)

translations : $(plugin_mo)

upgrade: 
	for i in $(plugin_pot); do msgmerge --width=110 --update $$i $(pot_file);done

clean :
	rm -f $(plugin_mo)

%.mo : %.po	
	msgfmt -o $@ $<

