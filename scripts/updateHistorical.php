<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
define('STATUSNET', true);
define('LACONICA', true); // compatibility
define('INSTALLDIR', realpath(dirname(__FILE__) . '/../../../..'));

require_once INSTALLDIR . '/lib/common.php';
require_once INSTALLDIR . '/local/plugins/Task/classes/Task_Grader.php';


$graderid = $_POST['graderid'];
$groupid = $_POST['groupid'];


$historical = Task_Grader::getHistorical($graderid, $groupid);


foreach ($historical as $taskHistory) {

    echo '<div id="task-' . $taskHistory['id'] . '" class="div-historical-task">';

    if ($taskHistory['status'] == 1) {
        $status = _m('Initiated');
    } else {
        $status = _m('Cancellled');
    }

    if ($taskHistory['tag'] == "") {

        $taskHistory['tag'] = '&lt;Ninguno&gt;';
    }

    echo '<p><span class="historical-bold">' . $status . '</span> '
    . '| <span class="historical-bold">'._m("Date").':</span> ' . $taskHistory['cdate'] . ' '
    . '| <span class="historical-bold">'._m("Tag:").'</span>  ' . $taskHistory['tag'] . ' '
    . '| <span class="historical-bold">'._m("Completed:").'</span>  '
    . $taskHistory['completed'] . '/' . $taskHistory['total'] . '</p>';

    if ($taskHistory['completed'] == 0) {

       if ($taskHistory['status'] == 1) {
            echo '<form action="' . common_local_url('taskcreate') . '" method="POST" class="ajax">';
            echo '<input type="hidden" value="' . $taskHistory['id'] . '" name="cancel-task"></input>';
            echo '<input type="submit" value="'._m('Cancel').'" class="cancel-task-button title="'._m("Cancel this task").'" onclick="updateTaskStatus(' . $taskHistory['id'] . ');"></input>';
            echo '</form>';
        } else {

            echo '<form action="' . common_local_url('taskcreate') . '" method="POST" class="ajax">';
            echo '<input type="hidden" value="' . $taskHistory['id'] . '" name="reopen-task"></input>';
            echo '<input type="submit" value="'._m('Init').'" class="reopen-task-button" title="'._m("Reopen this task").'" onclick="updateTaskStatus(' . $taskHistory['id'] . ');"></input>';
            echo '</form>';
       }
    }
    echo '</div>';
}
