<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es> and Jorge J. Gomez Sanz <jjgomez@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

class Task extends Managed_DataObject {

    public $__table = 'task';
    public $id = null; // id
    public $userid = null; // graderID
    public $status = null;

   static function staticGet($class="Task",$k, $v = null) {
        return Task::getKV( $k, $v);
    }



    /**
     * Data definition for email reminders
     */
    public static function schemaDef() {
        return array(
            'description' => 'Task Graders',
            'fields' => array(
                'id' => array(
                    'type' => 'serial',
                    'not null' => true,
                    'description' => 'Task ID'
                ),
                'userid' => array(
                    'type' => 'int',
                    'not null' => true,
                    'description' => 'ID del grader'
                ),
                'status' => array(
                    'type' => 'tinyint',
                    'not null' => true,
                    'description' => 'Status of Task'
                ),
            ),
            'primary key' => array('id', 'userid'),
        );
    }

    static function getPendingTasks($userid) {

        $task = new Task();

        $qry = 'select t.id as taskid '
                . 'from task t '
                . 'where t.userid = ' . $userid
                . ' and t.status = 0';

        $task->query($qry);

        $taskIds = array();

        while ($task->fetch()) {
            $taskIds[] = $task->taskid;
        }

        return $taskIds;
    }

    static function getNumberPendingTasks($userid) {

        $task = new Task();

        $qry = 'select count(t.id) as number '
                . 'from task t '
                . 'where t.userid = ' . $userid
                . ' and t.status = 0';

        $task->query($qry);
        if ($task->fetch()) {
            $number = $task->number;
        }

        return $number;
    }

    static function getNumberCompletedTasks($userid) {

        $task = new Task();

        $qry = 'select count(t.id) as number '
                . 'from task t '
                . 'where t.userid = ' . $userid
                . ' and t.status = 1';

        $task->query($qry);
        if ($task->fetch()) {
            $number = $task->number;
        }

        return $number;
    }

    static function getNumberRejectedTasks($userid) {

        $task = new Task();

        $qry = 'select count(t.id) as number '
                . 'from task t '
                . 'where t.userid = ' . $userid
                . ' and t.status = -1';

        $task->query($qry);
        if ($task->fetch()) {
            $number = $task->number;
        }

        return $number;
    }

    static function register($fields) {

        extract($fields);

        $task = new Task();

        $qry = 'INSERT INTO task (id,userid,status) values ';

        for ($i = 0; $i < count($idsUsers); $i++) {

            $qry .= '(' . $id . ',' . $idsUsers[$i] . ', 0)';

            if ($i < (count($idsUsers) - 1))
                $qry .= ', ';
        }

        $task->query($qry);
    }

    static function rejectTask($userid, $taskid) {

        $task = new Task();

        $qry = 'UPDATE task ' .
                'SET status=-1 ' .
                'WHERE userid=' . $userid .
                ' AND id=' . $taskid;

        $result = $task->query($qry);

        if (!$result) {
            common_log_db_error($user, 'UPDATE TASK FAILED for taskid '.$taskid.
                    ' and user id '.$userid.'. Either the user did not exist, '
                    . 'or the task id did not exist, or the task had already '
                    . 'the expected value');
        }

        $task->free();
    }

   static function deleteTask($userid, $taskid) {

        $task = new Task();

        $qry = 'DELETE task ' .
                'WHERE userid=' . $userid .
                ' AND id=' . $taskid;

        $result = $task->query($qry);

        if (!$result) {
            common_log_db_error($user, 'DELETE TASK', __FILE__);
        }

        $task->free();
    }


    static function completeTask($userid, $taskid) {

        $task = new Task();

        $qry = 'UPDATE task ' .
                'SET status=1 ' .
                'WHERE userid=' . $userid .
                ' AND id=' . $taskid;

        $result = $task->query($qry);

        if (!$result) {
            common_log_db_error($user, 'UPDATE TASK FAILED for taskid '.$taskid.
                    ' and user id '.$userid.'. Either the user did not exist, '
                    . 'or the task id did not exist, or the task had already '
                    . 'the expected value');
        }

        $task->free();
    }

    static function cancelTask($taskid) {

        $task = new Task();

        $qry = 'UPDATE task ' .
                'SET status=2 ' .
                'WHERE id=' . $taskid;

        $result = $task->query($qry);

        if (!$result) {
            common_log_db_error($user, 'UPDATE TASK FAILED for taskid '.$taskid.
                    ' and user id '.$userid.'. Either the user did not exist, '
                    . 'or the task id did not exist, or the task had already '
                    . 'the expected value');
        }

        $task->free();
    }

    static function reopenTask($taskid) {

        $task = new Task();

        $qry = 'UPDATE task ' .
                'SET status=0 ' .
                'WHERE id=' . $taskid;

        $result = $task->query($qry);

        if (!$result) {
            common_log_db_error($user, 'UPDATE TASK FAILED for taskid '.$taskid.
                    ' and user id '.$userid.'. Either the user did not exist, '
                    . 'or the task id did not exist, or the task had already '
                    . 'the expected value');
        }

        $task->free();
    }

}
