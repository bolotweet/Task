<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es> and Jorge J. Gomez Sanz <jjgomez@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/classes/Managed_DataObject.php';

class Task_Grader extends Managed_DataObject {

    public $__table = 'task_grader';
    public $id = null; // id
    public $graderid = null; // graderID
    public $groupid = null; // groupID
    public $cdate = null;

  static  function staticGet($class="Task_Grader",$k, $v = null) {
       return Task_Grader::getKV( $k, $v);
      
    }


   static function multiGet($keyCol, array $keyVals, $skipNulls = true) {
       
        return parent::multiGet( $keyCol, $keyVals, $skipNulls);
    }

    /**
     * Data definition for email reminders
     */
    public static function schemaDef() {
        return array(
            'description' => 'Task Graders',
            'fields' => array(
                'id' => array(
                    'type' => 'serial',
                    'not null' => true,
                    'description' => 'Task ID'
                ),
                'graderid' => array(
                    'type' => 'int',
                    'not null' => true,
                    'description' => 'ID del grader'
                ),
                'groupid' => array(
                    'type' => 'int',
                    'not null' => true,
                    'description' => 'Group ID'
                ),
                'tag' => array(
                    'type' => 'varchar',
                    'length' => 50,
                    'not null' => true,
                    'description' => 'Task tag'
                ),
                'status' => array(
                    'type' => 'tinyint',
                    'not null' => true,
                    'description' => 'Status of Task'
                ),
                'cdate' => array(
                    'type' => 'date',
                    'not null' => true,
                    'description' => 'Date the task was created'
                ),
            ),
            'primary key' => array('id'),
        );
    }

    static function register($fields) {

        extract($fields);

        $taskG = new Task_Grader();

        if ($tag == "") {
            $qry = 'INSERT INTO task_grader (graderid,groupid,cdate,status) '
                    . 'VALUES (' . $graderid . ',' . $groupid . ',CURDATE(), 1)';
        } else {

            $qry = 'INSERT INTO task_grader (graderid,groupid,tag,cdate,status) '
                    . 'VALUES (' . $graderid . ',' . $groupid . ',"' . $tag . '",CURDATE(), 1)';
        }

        $qry2 = 'SELECT LAST_INSERT_ID() as id FROM task_grader';

        $taskG->query('BEGIN');
        $taskG->query($qry);
        $taskG->query($qry2);
        $taskG->query('COMMIT');

        if ($taskG->fetch()) {
            $id = $taskG->id;
        }

        return $id;
    }

    static function cancel($taskid) {


        $task = new Task_Grader();

        $qry = 'UPDATE task_grader ' .
                'SET status=0 ' .
                'WHERE id=' . $taskid;

        $task->query($qry);

        $task->free();
    }

    static function reopenTask($taskid) {


        $task = new Task_Grader();

        $qry = 'UPDATE task_grader ' .
                'SET status=1 ' .
                'WHERE id=' . $taskid;

        $task->query($qry);

        $task->free();
    }

    static function updateTask($taskid, $tag) {

        $task = new Task_Grader();

        if ($tag == "") {
            $qry = 'UPDATE task_grader ' .
                    'SET status=1 ' .
                    'WHERE id=' . $taskid;
        } else {
            $qry = 'UPDATE task_grader ' .
                    'SET status=1, tag="' . $tag . '" ' .
                    'WHERE id=' . $taskid;
        }


        $task->query($qry);

        $task->free();
    }

    static function checkTask($graderid, $groupid) {


        $task = new Task_Grader();

        $qry = 'select tg.status as status, tg.id as id '
                . 'from task_grader tg '
                . 'where tg.graderid =  "' . $graderid . '"'
                . ' and tg.groupid = ' . $groupid
                . ' and tg.cdate = CURDATE()';


        $task->query($qry);

        if ($task->fetch()) {

            $result = array($task->status, $task->id);
        } else
            $result = -1;

        return $result;
    }

    static function getHistorical($graderid, $groupid) {

        $task = new Task_Grader();

        $qry = 'select ' .
                '(select count(id) from task where id = tg.id and status = 1) as completed, ' .
                '(select count(id) as total from task where id = tg.id) as total, ' .
                'tg.cdate as cdate, ' .
                'tg.status as status, ' .
                'tg.id as id, ' .
                'tg.tag as tag ' .
                'from task_grader tg ' .
                'where tg.graderid = ' . $graderid .
                ' and tg.groupid = ' . $groupid .
                ' order by tg.cdate desc';

        $task->query($qry);

        $historical = array();

        while ($task->fetch()) {
            $historical[] = array('completed' => $task->completed, 'total' => $task->total,
                'cdate' => $task->cdate, 'status' => $task->status, 'id' => $task->id, 'tag' => $task->tag);
        }

        return $historical;
    }

}
