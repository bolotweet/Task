<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/form.php';

class CancelForm extends Form {

    var $taskid = null;

    function __construct($out = null, $taskid = null) {
        parent::__construct($out);

        $this->taskid = $taskid;
    }

    /**
     * Action of the form
     *
     * @return string URL of the action
     */
    function action() {
        return common_local_url('taskcreate');
    }

    /**
     * Data elements
     *
     * @return void
     */
    function formData() {
        $this->out->hidden('cancel-task-h' . $this->taskid, $this->taskid, 'cancel-task');
        $this->out->element('input', array('type' => 'submit',
            'value' => _m('Cancel'), 'class' => 'cancel-task-button',
            'title' => _m('Cancel this task'),
            'onclick' => 'updateTaskStatus(' . $this->taskid .",'"._m('Initiated') ."','"._m('Cancelled') ."');"));
    }

    /**
     * Class of the form.
     *
     * @return string the form's class
     */
    function formClass() {
        return 'ajax';
    }

}
