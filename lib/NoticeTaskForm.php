<?php

/**
 * 
 *  Bolotweet-Task
    Copyright (C) 2018  bolotweet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author   Alvaro Ortego <alvorteg@ucm.es>
 *
 */
if (!defined('STATUSNET') && !defined('LACONICA')) {
    exit(1);
}

require_once INSTALLDIR . '/lib/form.php';

/**
 * Form for posting a notice
 *
 * Frequently-used form for posting a notice
 *
 * @category Form
 * @package  StatusNet
 * @author   Evan Prodromou <evan@status.net>
 * @author   Sarven Capadisli <csarven@status.net>
 * @license  http://www.fsf.org/licensing/licenses/agpl-3.0.html GNU Affero General Public License version 3.0
 * @link     http://status.net/
 *
 * @see      HTMLOutputter
 */
class NoticeTaskForm extends NoticeForm {


    var $taskid;

    /**
     * Constructor
     *
     * @param Action $action  Action we're being embedded into
     * @param array  $options Array of optional parameters
     *                        'user' a user instead of current
     *                        'content' notice content
     *                        'inreplyto' ID of notice to reply to
     *                        'lat' Latitude
     *                        'lon' Longitude
     *                        'location_id' ID of location
     *                        'location_ns' Namespace of location
     */
    function __construct($action, $taskid, $options1 = null, $msg = null) {
        // weirdly enough, if you leave $options here, the parameter passing fails
        // to the parent constructor
        parent::__construct($action,$options1);

        if ($msg != null) {
            $this->content = $msg;
        }

        $this->taskid = $taskid;

    }

    /**
     * ID of the form
     *
     * @return string ID of the form
     */
    function id() {
        return 'form_notice_task_' . $this->taskid;
    }

    /**
     * Class of the form
     *
     * @return string class of the form
     */
    function formClass() {
        return 'form_notice  tasky';
    }

    /**
     * Action of the form
     *
     * @return string URL of the action
     */
    function action() {
        return common_local_url('newnoticetask');
    }

    /**
     * Legend of the Form
     *
     * @return void
     */
    function formLegend() {
        // TRANS: Form legend for notice form.
        $this->out->element('legend', null, _('Send a notice'));
    }

    /**
     * Data elements
     *
     * @return void
     */
    function formData() {

        $this->out->hidden('hidden-group-id', $this->to_group->id, 'groupid');
        $this->out->hidden('hidden-task-id', $this->taskid, 'taskid');
        parent::formData();

    }
    
        protected function placeholderText()
    {
            return $this->content;
    }

    /**
     * Action elements
     *
     * @return void
     */
    function formActions() {
        $this->out->element('input', array('id' => 'submit-task-' . $this->taskid,
            'class' => 'submit task-submit-form',
            'name' => 'status_submit',
            'type' => 'submit',
            'onclick' => 'reducir(' . $this->taskid . ');',
            // TRANS: Button text for sending notice.
            'value' => _m('BUTTON', 'Send')));
    }

}
